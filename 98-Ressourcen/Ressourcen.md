# Ressourcen: Links & definitions for Ethereum (EN)

![Ethereum](../images/ethereum.png)

----
## Teil 1 links

Ruby ; Update ruby to lastest version on MacOs
http://codingpad.maryspad.com/2017/04/29/update-mac-os-x-to-the-current-version-of-ruby/

##  Teil 2 links

Remix Lokal: https://theethereum.wiki/w/index.php/Remix#Local_Installation

----
## Ethereum

* Home: 

blockchain on which you can build decentralized applications (smart contracts)
https://ethereum.org

* Ether:

Ethereum base cryptocurrency (base token).
https://ethereum.org/ether

* Geth:

client software provided by the Ethereum written in Go with the the following components:
-Daemon: downloads a copy of the blockchain, interacts with other nodes, mines,  validates & executes transactions,
 exposes API ( RPC) 
-Console: command line console, connect to an active nodes, and execute commands ( create accounts..)
https://ethereum.org/cli

* Mainnet: 

The live Ethereum blockchain which works using ether
https://www.ethernodes.org/network/1
https://etherscan.io

* Testnet:

A real Ethereum blockchain which works using test ethers and is used by developers for testing application. 
https://www.ethernodes.org/network/2

* MIST:

User freindly Ehtereum Client (GUI), includes geth
https://github.com/ethereum/mist

* TestRPC: 

a local simulation of an Ethereum Blockchain ( virtual)
https://github.com/ethereumjs/testrpc

* Web3.js:

javascript library to interact with the nodes and which eases the use of RPC 
https://github.com/ethereum/wiki/wiki/JavaScript-API

----
* Solidity:

programming language to write smart contracts on the Ethereum blockchain (compiled to Ethereum Virtual Machine byte code)

PDF:  https://media.readthedocs.org/pdf/solidity/develop/solidity.pdf

https://solidity.readthedocs.io/en/develop/

https://github.com/ConsenSys/smart-contract-best-practices


* Remix:
 
 A Browser based IDE for Solidity
 https://github.com/ethereum/browser-solidity
 
* DApp:

 A Smart Contract on the Blockchain + Application ( UI+logic usually in HTML + JavaScript)

 http://ethdocs.org/en/latest/contracts-and-transactions/developer-tools.html
 Live Dapps:
 https://www.stateofthedapps.com
 
 > Beispiel: FlightDelay Versicherung  https://fdd.etherisc.com

> https://github.com/etherisc/flightDelay/blob/master/contracts/FlightDelayLedger.sol

 * IPFS:  
 A peer-to-peer distributed file system 
 
 * Total Ether Supply:
 
 https://etherscan.io/stat/supply
 The current issuance rate is known: 5 ETH every ~15 seconds (Bitcoin is currently 25 BTC every ~10mins).
 https://www.coindesk.com/what-to-know-trading-ethereum/
 http://blockchain.info
 https://btc.com
 https://btc.com/stats/pool?pool_mode=year
 
 ----
 ## Blogs /Papers /Artikel /Tutorials 

* White Paper 

https://github.com/ethereum/wiki/wiki/White-Paper

* Yellow Paper

http://yellowpaper.io

* Gaz Price

    - https://docs.google.com/spreadsheets/d/1n6mRqkBz3iWcOlRem_mO09GtSKEKrAsfO7Frgx18pNU/edit#gid=0
    - https://ethgasstation.info


* Ethereum Live DApps

https://www.stateofthedapps.com/


* Ethereum Account Browser

https://www.etherchain.org/accounts

* Ethereum Blog

https://blog.ethereum.org


 * Tutorial Mist:
 
 http://remix.readthedocs.io/en/latest/tutorial_mist.html
 
* Artikel /Blogs:

Merkle Trees

https://blog.ethereum.org/2015/11/15/merkling-in-ethereum/

* web3

https://github.com/ethereum/wiki/wiki/JavaScript-API


 ```