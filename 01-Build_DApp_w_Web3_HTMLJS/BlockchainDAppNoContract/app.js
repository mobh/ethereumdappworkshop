const express = require('express');
var Web3 = require('web3');

const app = express();
app.use(express.json());

var web3;
web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

console.log('Eth Node Version: ', web3.version.node);
console.log("Network: " ,web3.version.network, web3.version.ethereum);
console.log('Connected to : ', web3.currentProvider);


// home
app.get('/', (req, res) => {
  var acc = web3.eth.accounts;
 res.send('<h1>Accounts</h1>  ' + acc);

});

// Start Server on PORT ( example, export PORT = 3000)
const port = 3001;
app.listen(port, () => {
  console.log(`Server started on ${port}!`);
});