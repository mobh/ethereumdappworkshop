
# Minimal DApp to access Ethereum Blockchain through Web3


```
mkdir Dapp
cd Dapp
```


## Installation  

### Node
 
Test Installation
Open up your command line or console and run the following 2 commands:

```
node -v
npm -v
```

If not installed, use the officail Installer ( no HomeBew..because of verion Issues on npm and node)


Install testRpc globally

```
npm install -g ethereumjs-testrpc
```

Once finished, run the following command to start it:

```
testrpc
```

This provides you with 10 different accounts and private keys, along with a local server at localhost:8545.



### Installing Express Web3.js  

Edit package.json and add web3 and express


### adapt app.js and start Server  
