

![Technische Einführung](../images/Slide_Teil1.png)

----
![HandsOn](../images/Theorie.png)

----
Vorkenntnisse

- HTML & CSS
- JavaScript Grundlagen


----
----
## Create Project Directory

```
mkdir eventshop
cd eventshop
```


## Installation  

### Node
 
Test Installation
Open up your command line or console and run the following 2 commands:

```
node -v
npm -v
```

If not installed, use the officail Installer ( no HomeBew..because of verion Issues on npm and node)


Install testRpc globally

```
npm install -g ethereumjs-testrpc
```

Once finished, run the following command to start it:

```
testrpc
```

This provides you with 10 different accounts and private keys, along with a local server at localhost:8545.




### Installing Web3.js  


Next, run the npm init command to create a package.json file, which will store project dependencies:

```
npm init

```

install web3.js

```
npm install ethereum/web3.js --save

```

### Deploy the contract using Remix

-Switch to the Remix IDE  
-Change envirionment to testrpc
click on the Run tab, and then change the Environment dropdown from Javascript VM to Web3 Provider.

Hit "OK" and then specify the testrpc localhost address (by default, it's http://localhost:8545)

This means that instead of deploying and testing in the Javascript VM, we're now using the TestRPC client on your computer.

```
pragma solidity ^0.4.18;

contract EventShop { 

	address public owner;
	
	modifier isOwner(){
	    require (msg.sender == owner);
	    _;
	}

	struct Event {
        string name;
        uint uid;
        uint price;
        uint ticketStock;
       
    } 

    // Constructor
    function EventShop() public {
    	owner  = msg.sender;
    }

    mapping(uint => Event) public events;

    // Only owner can add Events
    function addEvent(string _name, uint _uid, uint _price, uint _ticketStock ) public returns (bool success) {
    	
        if (msg.sender == owner){
        	Event memory newEvent  = Event({ name: _name, uid: _uid , price: _price, ticketStock: _ticketStock });
	    	events[_uid] =  newEvent;
	    	return true;
	    }else{
        	revert();

        }
        return false;
    }

    // Anyone can register to events  ( reading and writing to the storage costs )

   function registerToEvent(uint _eventId)  public payable {
     
       require ( msg.value >= events[_eventId].price);
       Event memory lookupEvent = events[_eventId]; 
        uint rest;    

        if (lookupEvent.uid > 0 && msg.value >= lookupEvent.price && lookupEvent.ticketStock > 0 ){
        	//adapt stock
        	lookupEvent.ticketStock --;
        	rest = msg.value - lookupEvent.price;
        	 // give the Money back to sender
           	if (rest > 0){
			    msg.sender.transfer(rest);
			}
        	
        }else{
           

        }
     }	

    // Owner can withdraw the Money in the contract 
     function withdraw() isOwner public{
         // Use Transfer ( new method)
         owner.transfer(this.balance); 
     }	

}

```


## Creating a Web UI for a Smart Contract

-start VisualCode ( or your Favorite Editor)
Open up your preferred code editor (I use Visual Studio Code) with the project folder we created. Here, you'll notice a node_modules folder, which includes web3 that we installed via npm earlier.

-Create index.html
Let's create an index.html in the project folder.
a simple CSS, and a UI to addProducts and buyProduct , we  will use jQuery to simplify the use of JS.

To get started, paste the following contents into the empty index.html file:

```
<!DOCTYPE html>
<html>
<head>
  <title>Event Shop , Pay your Events with Crypto </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="StyleSheets/app.css">
  <!--<script src="JavaScript/app.js"></script>-->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="../node_modules/web3/dist/web3.min.js"></script>


</head>
<body>
  <h1>Event Shop</h1>
  <h2>Pay your Events with Crypto</h2>
  <h3>You have <span class="black"><span id="balance"></span> Ether</span></h3>


  <h2>Our Events</h2>
  <hr>
  <span id="events"></span>

 
  <!-- This Section should be shown only to owner-->
  <div id="ownerSection">

   
   <br>
  <h1>Add Event</h1>
    <br><label for="name">Name:</label><input type="text" id="name" placeholder="Blockchain Workshop"></input>
  <br><label for="price">Price:</label><input type="text" id="price" placeholder="e.g., 1 Ether"></input>
  <br><label for="ticketStock">Ticket Stock:</label><input type="text" id="ticketStock" placeholder="e.g., 10"></input>
  <p>Event UID = Hash of an Address of Owner + Counter</p>
  <br><label for="uid">Event Uid:</label><input type="text" id="uid" placeholder="Hash will be calculated"></input>
  
  <br><br><button id="addButton">Add Event</button>
  </div>
  <br><br>

  <br>

 <div id="buyerSection">
  <h1>Register to Event</h1>
  <br><label for="uid">Event Uid:</label><input type="text" id="uid" placeholder="Choose Event Uid"></input>
  <br><br><button id="send" onclick="eventShopContract.registerToEvent()">Register to Event</button>
  <br><br>
  </div>
  <br>

  <span id="status"></span>
  <span class="hint"><strong>Hint:</strong> open the  browser developer console to view any errors and warnings.</span>

<script>

          if (typeof web3 !== 'undefined') {
              web3 = new Web3(web3.currentProvider);
          } else {
              // set the provider you want from Web3.providers
              web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
          }

          web3.eth.defaultAccount = web3.eth.accounts[0];
          var eventShopABI = web3.eth.contract([{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"loyaliyCoins","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"events","outputs":[{"name":"uid","type":"uint256"},{"name":"name","type":"string"},{"name":"price","type":"uint256"},{"name":"ticketStock","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_uid","type":"uint256"}],"name":"buyWithToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_uid","type":"uint256"}],"name":"registerToEvent","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_uid","type":"uint256"},{"name":"_name","type":"string"},{"name":"_price","type":"uint256"},{"name":"_stock","type":"uint256"}],"name":"addEvent","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]);

  var eventShopContract = eventShopABI.at('0x7c56b2a4bdaa13708de2b1fd3d45172b723882dd');
  console.log(eventShopContract);
  eventShopContract.events(1, function(error, result){
           if(!error)
               {
                   $("#events").html('Event:'+result);
                   console.log(result);
               }
           else
               console.error(error);
       });
/*
      $("#addButton").click(function() {
        console.log("Hi");

       });
  */
       $("#addButton").click(function() {
            eventShopContract.addEvent($("#uid").val(), $("#name").val(), $("#price").val(), $("#ticketStock").val(), {from: web3.eth.accounts[0], gas:3000000}, function(error, result){
                     if(!error)
                         {
                             $("#events").html('Event:'+result);
                             console.log(result);
                         }
                     else
                         console.error(error);
                 });

       });


  </script>

</body>
</html>


```

We are using the following CSS FIle in the HTML Pages

```
body {
  margin-left: 25%;
  margin-right: 25%;
  margin-top: 10%;
  font-family: "Open Sans", sans-serif;
}

label {
  display: inline-block;
  width: 100px;
}

input {
  width: 500px;
  padding: 5px;
  font-size: 16px;
}

button {
  font-size: 16px;
  padding: 5px;
}

h1, h2 {
  display: inline-block;
  vertical-align: middle;
  margin-top: 0px;
  margin-bottom: 10px;
}

h2 {
  color: #AAA;
  font-size: 32px;
}

h3 {
  font-weight: normal;
  color: #AAA;
  font-size: 24px;
}

.black {
  color: black;
}

#balance {
  color: black;
}

.hint {
  color: #666;
}


```



### Use Web3.js to Connect & Interact with the Smart Contract

-configure web3
In the head tags, we're already importing the Web3.js library, so now, let's use it to connect to our testrpc client:

```
  if (typeof web3 !== 'undefined') {
              web3 = new Web3(web3.currentProvider);
          } else {
              // set the provider you want from Web3.providers
              web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
          }


```
It's saying that if web3 is not undefined, then we'll use that as our provider. If it's undefined (else), we can manually specify the provider ourselves.

You may be wondering, how would web3 be defined? Well, if you're using the Chrome extension MetaMask (which we will use later in this course) or an Ethereum browser like Mist, the provider is automatically injected.


-specify a default ethereum account
Remember when we ran the testrpc console command? It provided us with 10 accounts. We're simply choosing the first account here to use.
```
        web3.eth.defaultAccount = web3.eth.accounts[0];

```
-initialize the contract
Next, we need to use the web3.eth.contract() method to initiatlize (or create) the contract on an address. It accepts one parameter, which is referred to as the ABI (Application Binary Interface). 

If you switch back to the Remix IDE, click on the Compile tab and click Details. Scroll down until you see the Interface - ABI section and click the copy icon as shown below:

```
     web3.eth.defaultAccount = web3.eth.accounts[0];
    var eventShopABI =   web3.eth.contract(ABI HERE!);  

```

-define the actual contract address. 

We used Remix to create the contract earlier, and it has an associated address. 

Go back to Remix and click the Run tab, and click on the copy icon next to the contract that we created earlier on the right column.

```
     web3.eth.defaultAccount = web3.eth.accounts[0];
    var eventShopABI =   web3.eth.contract(ABI HERE!);  

```

-copy the contract address from remix 

Go back to Remix and click the Run tab, and click on the copy icon next to the contract that we created earlier on the right column.

Back in index.html add the following line:

```
    var eventShopContract = eventShopABI.at('0x7c56b2a4bdaa13708de2b1fd3d45172b723882dd');
  console.log(eventShopContract);

```

### Test the calls in JavaScript console
-save the index.html page and open it in the CHrome Browser
- activate the JS COnsole in the Browser and test the calls

```
   > Coursetro.setInstructor('Brutis', 44) // Hit Enter
"0x894..."                           // This is the response (address)

> Coursetro.getInstructor()          // Hit Enter
(2) ["brutis", e]                    // An array containing our data

```
### Add the calls in HTML Page (using JavaScript)

Let's use Javasciort & jQuery to make these calls for us based on our form:

```
   $("#addButton").click(function() {
            eventShopContract.addEvent($("#uid").val(), $("#name").val(), $("#price").val(), $("#ticketStock").val(), {from: web3.eth.accounts[0], gas:3000000}, function(error, result){
                     if(!error)
                         {
                             $("#events").html('Event:'+result);
                             console.log(result);
                         }
                     else
                         console.error(error);
                 });

       });

```

We're simply calling .getInstructor and passing the error and result through a callback function. If the error isn't present, we set the html of an h2 element with the id of #instructor to the returned result array (0 = the name, 1 = the age).

Next, on click, we call .setInstructor to the name and age values from the input fields in the form.

Save it, refresh and give it a go!



### Defining the Smart Contract Event

An event occurs when we set the Instructor's name. Let's define one in the contract just above the setInstructor() function:

```
event Instructor(
       string name,
       uint age
    );
```


### Defining the Smart Contract Event

An event occurs when we set the Instructor's name. Let's define one in the contract just above the setInstructor() function:

```


```


## useful Links 

JSON formatter for Comapct JSON without tab formatting
https://jsonformatter.curiousconcept.com









