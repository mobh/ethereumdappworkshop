var SimpleShop = artifacts.require("./SimpleShop.sol");


contract('SimpleShop', function(accounts) {
    it("should add 1 Product", function() {
      var shop;
      //return SimpleShop.new({from:accounts[0]}).
      return SimpleShop.deployed().
           then(function(instance) {
          shop = instance;
          return instance.addProduct("Tesla", 2, 10000,10, {from: accounts[0]});
       }).then(function(txObject) {
          // has event
           return shop.products(2);
        }).then(function(result)  {
          console.log (result);
           console.log (result[0]);
          assert.equal(result[0] , "Tesla");
        });
      });

      it("should substract product from Stock", function() {
        var shop;
        return SimpleShop.deployed().
             then(function(instance) {
            shop = instance;
            return instance.buyProduct( 2, {value: 10000});
         }).then(function(txObject) {
            // has event
             return shop.products(2);
          }).then(function(result)  {
            console.log (result);
            console.log (result[0]);
            assert.equal(result[3] , 9);
          });
        });

        it("should fail, price too small", function() {
          var shop;
          return SimpleShop.deployed().
               then(function(instance) {
              shop = instance;
              return instance.buyProduct( 2, {value: 9999});
            }).then(function(returnValue) {
              assert(false, "buyProduct was supposed to throw exception !");
            }).catch(function(error) {
                // exception
                assert(true, error.toString());
              
            });
          });
  });


